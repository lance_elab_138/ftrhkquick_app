import React from 'react';

export default function CupFilpper(){
    var [inCup,setInCup] = React.useState('');
    var [outCup,setOutCup] = React.useState('');
    var [mode,setCupMode] = React.useState('Hybris');
    const copyToClipboardClick = (e)=>{navigator.clipboard.writeText(e.currentTarget.value)};
    const changeModeAndList =(e)=>{
        setCupMode(e.currentTarget.value)
        changeList(inCup,e.currentTarget.value);
    }
    const changeList = (e,emode)=>{
        if(e === null) {return;}
        var str;
        console.log(e);
        if(!(e instanceof String) && e.currentTarget instanceof HTMLTextAreaElement){
            str = e.currentTarget.value;
            setInCup(e.currentTarget.value);
        } else {
            str = inCup;
        }
        var curmode = emode? emode:mode;
        switch(curmode){
            case "Hybris":
                str = str.split('\n').filter(x=>x!='').map(x=>{return `'${x}'`}).toString().replace(/\,/gm,",\n");
                break;
            case "Java/Groovy":
                str = "["+str.split('\n').filter(x=>x!='').map(x=>{return `"${x}"`}).toString().replace(/\,/gm,",\n")+"]";
                break;
            case "One-line comma-delimit":
                str = str.split('\n').filter(x=>x!='').toString();
                break;
        }
        setOutCup(str)
    }
    return(
        <div id="cupFilpper">
            <h2>Enter entries batch and pick the filpping mode.</h2>
            <select type="radio" defaultValue="Hybris" onChange={changeModeAndList}>
                <option>Hybris</option>
                <option>Java/Groovy</option>
                <option>One-line comma-delimit</option>
            </select>
            <div class="textio">
                <textarea className="input" onChange={changeList}></textarea>
                <textarea className="output" value={outCup} onClick={copyToClipboardClick}></textarea>
            </div>
        </div>
    );
}