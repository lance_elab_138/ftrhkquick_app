import React from 'react';
import axios from 'axios';





export default function FlexibleQuery(){
    var [queryIndex,setQueryIndex] = React.useState(0);
    var [queryPlacement,setQueryPlacement] = React.useState('');
    var [target,setTarget] = React.useState('')
    var [fsqList,setFsqList] = React.useState([]);
    var [fsqTitle,setfsqTitle] = React.useState(0);
    var [batch,setBatch] = React.useState('');
    React.useEffect(async()=>{
        const getFSQList=  async()=>{
            var fsqlist = (await axios.get('http://localhost:5000/fsq')).data;
            console.log(fsqlist)
            setFsqList(fsqlist);
            setQueryPlacement(fsqlist.map((x)=>{return x.flexibleQuery;}));
        }
        await getFSQList();
        } ,[])
    const changePlacement = (o)=>{
        console.log(o)
        var queryTitle = o.title? o.title:fsqTitle;
        var queryStr = fsqList.find(x=>x.title===queryTitle).flexibleQuery;
        queryStr =
            queryStr
                .replace('[[target]]',o.target)
                .replace('[[dateStart]]',o.dateStart?o.dateStart.replace('T',' ')+':00':'[[dateStart]]')
                .replace('[[Batch]]',o.batch?o.batch.split('\n').filter(x=>x!='').map(x=>{return `'${x}'`}).toString().replace(/\,/gm,",\n"):'[[Batch]]')
                .replace('[[models]]',o.batch?o.batch.split('\n').filter(x=>x!='').map(x=>{return `'${x}'`}).toString().replace(/\,/gm,",\n"):'[[Batch]]')
        setQueryPlacement(queryStr);
        setfsqTitle(queryTitle);
    }
    const type = ["basic","user","sales","misc"];
    var QueryRow = (p)=>{
        return(
            <tr className="QueryRow" id={p.type}>
                <td>{p.type}</td>
                <td>
                    {fsqList.filter(x=>x.type===p.type).map((x,i)=>{
                        return(
                            <button
                                id={x.title}
                                key={i}
                                name="queryType"
                                data-index={i}
                                onClick={(e)=>{changePlacement({title:x.title})}} >{x.title}</button>
                        )
                    })
                    }
                </td>
            </tr>
        )
    };
    return (
        <div>
            <table id="QueryPicker">
                {type.map((x,i)=>{
                    return(<QueryRow type={x} />)
                })}
            </table>
            <table>
                <tr>
                    <td>Target</td>
                    <td>
                        <input type="text" onChange={(e)=>{changePlacement({target:e.currentTarget.value});}}/>
                    </td>
                </tr>
                <tr>
                    <td>DateStart</td>
                    <td>
                        <input type="datetime-local" onChange={(e)=>{changePlacement({dateStart:e.currentTarget.value});}}/>
                    </td>
                </tr>
                <tr>
                    <td>Batch (newline for entries)</td>
                    <td>
                        <textarea id='queryBatch' onChange={(e)=>{changePlacement({batch:e.currentTarget.value});}}/>
                    </td>
                </tr>
            </table>
            <textarea
                id="queryField"
                value={queryPlacement}
                onChange={(e)=>setQueryPlacement(e.currentTarget.value)}
                onClick={(e)=>{navigator.clipboard.writeText(e.currentTarget.value)}}>
            </textarea>
        </div>
    );
}