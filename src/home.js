import React from 'react';
import PartialCancel from "./PartialCancel";

export default function Home(){
    let dateString = new Date().toISOString().substr(0,10).replace(/-/g,"")
    let [jsDate,setJSDate] = React.useState(new Date());
    let [yyyymmdd,setYMD] = React.useState(dateString);
    let [yyyymm,setYM] = React.useState(yyyymmdd.substring(0,6));;

    const isGZ = (v)=>{
        const gzdate = v || jsDate;
        return new Date() - gzdate > 86400000*4? '.log.gz':'.log';
    }
    let [logOutPut,setLogOut] = React.useState('');

    let [nsjCode,setNSJCode] = React.useState('');
    let [prtCode,setPRTCode] = React.useState('[[productCode]]');
    let [grep,setGrep] = React.useState('[[target]]');
    let [gxCode,setGXCode] = React.useState('[[target]]');
    let [nsjCheckCmd,setNsjCmd] = React.useState(`grep 'NightlySyncJob' ~/logs/console-${yyyymmdd}${isGZ()}\n`);
    let [nsjTailCmd,setTailCmd] = React.useState(`tail -f ~/logs/console-${yyyymmdd}${isGZ()} | grep '${nsjCode}'\n`);
    let [priceReconCmd,setPrtCmd] = React.useState(`cyia \ngrep '${prtCode}' IS31PR_RET_FTRHK_${yyyymm}* \ngrep '[[productCode]]' IS31MD_RET_FTRHK_${yyyymm}*\n`);
    let [grepCmd,setGrepCmd] = React.useState(`grep '${grep}' ~/logs/console-${yyyymmdd}${isGZ()}\n`);
    let [grepXCmd,setGXCmd] = React.useState(`~/grepX/grepX.sh '${gxCode}' ~/logs/console-${yyyymmdd}${isGZ()}\n`);
    let cronJobContent = [
        {
            name:'NightlySyncJob',
            description:'Command line to check NightlySyncJob.\nEnter jobCode to check specific cronJob (0000XXXX)'
        },
        {
            name:'Tail Current NightlySyncJob',
            description:'Command line to TAIL NightlySyncJob.\nEnter jobCode to check specific cronJob (0000XXXX)\n(Empty grep can be used to tail console directly)'
        },
        {
            name:'priceReconAlert',
            description:'Check priceRecon with the given Product Code'
        },
        {
            name:'grep',
            description:'grep on current FTRHK server within given wording'
        },
        {
            name:'grepX',
            description:'grepX on all FTRHK servers within given wording'
        }
    ]
    let [jobContent,setContent] = React.useState(cronJobContent);
    let commands =[
        nsjCheckCmd,
        nsjTailCmd,
        priceReconCmd,
        grepCmd,
        grepXCmd
    ];
    const TaskRow = (p) =>{
        return(
            <tr id={p.task.name}>
                <td>{p.task.name}</td>
                <td onClick={(e)=>{navigator.clipboard.writeText(e.currentTarget.innerText)}}>
                    {commands[p.index]}
                </td>
                <td>{p.task.description}</td>
            </tr>
        )
    }
    const DateChange = (jsdate,date,sdate)=>{
        setNsjCmd(`grep 'NightlySyncJob' ~/logs/console-${date}${isGZ(jsdate)}`+(nsjCode?`\ngrep '${nsjCode}' ~/logs/console-${date}${isGZ(jsdate)}\n`:''));
        setTailCmd(`tail -f ~/logs/console-${date}${isGZ(jsdate)} | grep '${nsjCode}'\n`)
        setPrtCmd(`cyia \ngrep '${prtCode}' IS31PR_RET_FTRHK_${sdate}* \ngrep '${prtCode}' IS31MD_RET_FTRHK_${sdate}*\n`);
        setGrepCmd(`grep '${grep}' ~/logs/console-${date}${isGZ(jsdate)}\n`);
        setGXCmd(`~/grepX/grepX.sh '${gxCode}' ~/logs/console-${date}${isGZ(jsdate)}\n`);
    }

    const setTargetDate = (v)=>{
        let dateString = new Date(v).toISOString().substr(0,10).replace(/-/g,"")
        setJSDate(new Date(v));
        setYMD(dateString);
        setYM(yyyymmdd.substring(0,6));
        DateChange(new Date(v),dateString,dateString.substring(0,6));
    }
    const setNSJ = (v)=>{
        setNsjCmd(`grep 'NightlySyncJob' ~/logs/console-${yyyymmdd}${isGZ()}`+(v?`\ngrep '${v}' ~/logs/console-${yyyymmdd}${isGZ()}\n`:''));
        setTailCmd(`tail -f ~/logs/console-${yyyymmdd}${isGZ()} | grep '${v}'\n`);
        setNSJCode(v);
    }
    const setPRT = (v)=>{
        setPrtCmd(`cyia \ngrep '${v}' IS31PR_RET_FTRHK_${yyyymm}* \ngrep '${v}' IS31MD_RET_FTRHK_${yyyymm}*\n`);
        setPRTCode(v);
    }
    const setGREP = (v)=>{
        setGrepCmd(`grep '${v}' ~/logs/console-${yyyymmdd}${isGZ()}\n`);
        setGrep(v)
    }
    const changeGX = (v)=>{
        let gz =
        setGXCmd(`~/grepX/grepX.sh '${v}' ~/logs/console-${yyyymmdd}${isGZ()}\n`)
        setGXCode(v);
    }
    const logParser = (v)=>{
        if(!v || v === '' || v.length === 0 || v === null){return;}

        let logOut = '';
        let jobGrep = '## Grep These Job ##\n\n';

        let log = v;
        let jobCode = log.match(/(?<=\').{8}(?=\')/gm);

        if(jobCode instanceof Array){
            let jobDetails = jobCode.map((x,i)=>{
                return {
                    code:x,
                    created:log.match(new RegExp(`(?<=jvm 1    \\| main    \\| ).+(?= \\| INFO.+ Sync.+${x})`,'gm')) || 'UNKNOWN',
                    entries:log.match(new RegExp(`(?<=configured )\\d*(?= entries for job '${x}')`,'gm')) || 'UNKNOWN',
                    catalogJob:log.match(new RegExp(`(?<=Sync  \\').*?(?=\\'.+\\'${x})`,'gm')) || 'UNKNOWN',
                    lastline:log.match(new RegExp(`.+${x}.+`,'gm'))[log.match(new RegExp(`.+${x}.+`,'gm')).length-1] || ''

                }
            });
            jobGrep +=
                jobCode.map((x)=>{return `grep '${x}' ~/logs/console-${yyyymmdd}.log`}).join(" &&\n") + '\n\n\n' +
                jobDetails.map((x)=>{
                    return (`JobCode: ${x.code}\nCreated: ${x.created}\nEntries: ${x.entries}\ncatalogJob: ${x.catalogJob}\nlastline:\n\n ${x.lastline}`)
                }).join('\n\n\n');
            if(log.match('productFlow') === null){
                jobGrep +=  "\n#### NO PRODUCTFLOWLOADER ####"
            }
            jobDetails.forEach((x,i)=>{
                if(i<2 && x.lastline.includes("Finished")){
                    let timeArray = x.lastline.match(/\d+\:\d+\:\d+/gm);
                    if((parseInt(timeArray[0]) == 8 && parseInt(timeArray[1]) >=30 && parseInt(timeArray[2]) > 0 )|| parseInt(timeArray[0]) >=9){
                        jobGrep +=  "\n#### RUN SOLR INDEXER ####"
                    }
                }
            });

        }
        logOut += jobGrep;
        setLogOut(logOut)
    }
    return(
        <div id="home">
            <div className="vWrap">
                <div>
                    <h2>FTRHK Quick Toolbox</h2>
                    <h3>CronJob Checking Command</h3>
                    <p>Click on cheat command to copy command instantly.</p>
                    <table>


                        <tr className="trackingField">
                            <td>
                                <span>Date Logging</span>
                            </td>
                            <td>
                                <input type="date" defaultValue={new Date().toISOString().substr(0,10).replace(/\//g,"")} onChange={(e)=>{setTargetDate(e.target.value);}}/>
                            </td>
                        </tr>
                        <tr className="trackingField">
                            <td>
                                <span>NightlySyncJobCode</span>
                            </td>
                            <td>
                                <input type="text" onChange={(e)=>{setNSJ(e.target.value);}}/>
                            </td>
                        </tr>
                        <tr className="trackingField">
                            <td>
                                <span>price</span>
                            </td>
                            <td>
                                <input type="text" onChange={(e)=>{setPRT(e.target.value);}}/>
                            </td>
                        </tr>
                        <tr className="trackingField">
                            <td>
                                <span>grep</span>
                            </td>
                            <td>
                                <input type="text" onChange={(e)=>{setGREP(e.target.value);}}/>
                            </td>
                        </tr>
                        <tr className="trackingField">
                            <td>
                                <span>grepX</span>
                            </td>
                            <td>
                                <input type="text" onChange={(e)=>{changeGX(e.target.value);}}/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <table className="genericTable">
                        <thead>
                        <td>Cheat Name</td>
                        <td>Cheat Commands</td>
                        <td>Description</td>
                        </thead>
                        {jobContent.map((x,i)=> { return (<TaskRow name={x.name} task={x} index={i}/>)})}
                    </table>
                </div>
            </div>
            <div id="NightlySyncParser">
                <h3>Enter NightlySyncJob Grep Log on the left.</h3>
                <div className="textio">
                    <textarea className="logInput" onChange={(e)=>{logParser(e.currentTarget.value)}}></textarea>
                    <textarea className="logOutput" onChange={(e)=>setLogOut(e.currentTarget.value)} value={logOutPut}></textarea>
                </div>
            </div>
        </div>
    );
}