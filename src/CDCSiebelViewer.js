import React from "react";
import axios from 'axios';
import textAttr from "./textAttr";

export default function CDCSiebelViewer(){
    var [MBID,setMBID] = React.useState('');
    var [queryDataInfo,setqueryDataInfo] = React.useState({});
    var [queryStatusInfo,setqueryStatusInfo] = React.useState({});
    const CDCRequest = async(e)=>{
        var siebelMessage = await (await axios.get('http://localhost:5000/cdc/'+MBID)).data["SiebelMessage"];
        console.log(siebelMessage)
        setqueryDataInfo(siebelMessage["Body"]?siebelMessage["Body"]["Customer"]:{});
        setqueryStatusInfo(siebelMessage["Header"]);
        console.log(queryStatusInfo);
    }
    const Summary = (p)=>{
        if(!p.payload){return (<></>)}
        let customerData = p.payload;
        let filterData = []
        console.log(customerData["MemberNumber"]);
        return (
            <div id="customerSummary">
                {Object.entries(customerData).map(([k,x])=>{
                    if(k.match(/Flag/)){return;}
                    return (
                        <div class="SummaryData">
                            <div>{k}</div>
                            <div>{textAttr(x)}</div>
                        </div>
                    )
                })}
            </div>
        )
    }
    const Segments = (p)=>{
        if(!p.payload["Segments"]){return (<></>)}
        let segmentData = p.payload["Segments"]["Segment"];
        console.log(segmentData);
        return (
            <table>
                <thead>
                {Object.keys(segmentData[0]).map((x)=>{ return (<td>{x}</td>)})}
                </thead>
                {segmentData.map((x)=>{return(
                    <tr>
                        {Object.values(x).map((y)=>{
                            return (<td>{textAttr(y)}</td>)
                        })}
                    </tr>
                )})}
            </table>)
    }
    const Accounts = (p)=>{
        if(!p.payload["Accounts"]){return (<></>)}
        let accountData = p.payload["Accounts"]["Account"];
        console.log(accountData);
        return (
            <table>
                <thead>
                {Object.keys(accountData[0]).map((x)=>{ return (<td>{x}</td>)})}
                </thead>
                {accountData.map((x)=>{return(
                    <tr>
                        {Object.values(x).map((y)=>{
                            return (<td>{textAttr(y)}</td>)
                        })}
                    </tr>
                )})}
            </table>)
    }
    const QueryData = (p)=>{
        var [dataView,setDataView] = React.useState(<Summary payload={p.payload}/>);
        return (
            <div id="memberData">
                <ul>
                    <li onClick={(e)=>{setDataView(<Summary payload={p.payload}/>)}}>Summary</li>
                    <li onClick={(e)=>{setDataView(<Segments payload={p.payload}/>)}}>Segments</li>
                    <li onClick={(e)=>{setDataView(<Accounts payload={p.payload}/>)}}>Accounts</li>
                </ul>
                <div id="viewport">
                    {dataView}
                </div>
            </div>
        )
    }
    const QueryStatus = (p)=>{
        var payload = p.payload;
        console.log(payload);
        if(!payload) {
           return( <div id="queryStatus">
            </div>)
        }
        return (
            <div id="queryStatus">
                {Object.entries(payload).map(([k,v])=>{
                    if(k==='Error'){
                        return  (<div> Error {Object.keys(v)[0]}: {textAttr(v['Code'])}</div>)
                    }
                    return (
                        <div>{k}: {textAttr(v)}</div>
                    );
                })}
            </div>
        )

    }

    return (
        <div id="CDCSiebelViewer">
            <input type="text" onChange={(e)=>{setMBID(e.currentTarget.value)}}/>
            <button onClick={CDCRequest}>Search</button>
            <QueryStatus payload={queryStatusInfo}/>
            <QueryData payload={queryDataInfo}/>
        </div>
    )
}