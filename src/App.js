import logo from './logo.svg';
import './App.css';
import React from 'react';

import  Home from './home';
import LinkHopper from "./LinkHopper";
import FlexibleQuery from './FlexibleQuery'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import PartialCancel from "./PartialCancel";
import CupFilpper from "./CupFilpper";
import CDCSiebelViewer from "./CDCSiebelViewer";
function App() {
  return (
    <Router>
        <div id="header">
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/linkHopper">linkHopper</Link></li>
                <li><Link to="/cupFilpper">cupFilpper</Link></li>
                <li><Link to="/CDCSiebelViewer">CDCSiebelViewer</Link></li>
                <li><Link to="/flexibleQuery">FlexibleQuery</Link></li>
                <li><Link to="/PartialCancel">PartialCancel</Link></li>
            </ul>
        </div>
        <Switch>
            <div id="mainView">
                <Route exact path="/CDCSiebelViewer">
                    <CDCSiebelViewer/>
                </Route>
                <Route exact path="/cupFilpper">
                    <CupFilpper/>
                </Route>
                <Route exact path="/PartialCancel">
                    <PartialCancel/>
                </Route>
                <Route exact path="/flexibleQuery">
                    <FlexibleQuery/>
                </Route>
                <Route exact path="/linkHopper">
                    <LinkHopper/>
                </Route>
                <Route exact path="/">
                    <Home/>
                </Route>
            </div>
        </Switch>
    </Router>
  );
}

export default App;
