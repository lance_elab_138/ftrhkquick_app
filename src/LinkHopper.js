import React from 'react';
export default function LinkHopper(){
    const ip = ["10.32.36.174",
        "10.32.183.91",
        "infftrhkuat.aswatson.net",
        "10.32.36.175",
        "10.32.36.176",
        "10.32.36.177",
        "10.32.36.178",
        "10.32.36.179",
        "10.32.36.188",
        "10.32.36.189",
        "10.32.36.190",
        "10.32.36.191",
        "10.32.36.51",
        "10.32.36.52",
        "10.32.36.53",
        "10.32.36.54",
        "10.32.36.55",
        "10.32.36.105",
        "10.32.36.106",
        "10.32.36.107",
        "10.32.36.108",
        "10.32.36.109",
        "10.32.36.22",
        "10.32.36.23",
        "10.32.36.27",
        "10.32.36.28",
        "10.32.36.80",
        "10.32.36.81",
        "10.32.36.82",
        "10.32.36.83",
        "10.32.36.84",
        "10.32.36.85",
        "10.32.36.79",
        "10.32.36.86",
        "10.32.36.87",
        "10.32.36.88",
        "10.32.36.89",
        "10.32.36.90",
        "10.32.36.91",
        "10.32.36.92",
        "10.32.36.93",
        "10.32.36.94",
        "10.32.36.95",
        "10.32.36.96",
        "10.32.36.97",
        "10.32.36.98",
        "10.32.36.99",
        "10.32.183.144"
    ]
    let mainPoint = ["10.32.36.174"];
    let uatPoint = ["10.32.183.91","infftrhkuat.aswatson.net",];
    let sitPoint = ["10.32.183.144"]
    let adminPoint = mainPoint.concat(uatPoint).concat(sitPoint);
    let [url,setURL] = React.useState('https://10.32.36.174:9002/');
    let [dest,setDest] = React.useState('10.32.36.174');
    let [PC,setPC] = React.useState('');
    let [cnum, setNumber] = React.useState('');
    let [prodStatus,setProdStatus] = React.useState(true);;
    let [catStatus,setCatStatus] = React.useState(true);
    let [access,setAccess] = React.useState(false);
    let [def,setDefStatus] = React.useState(true);
    const URLBuilder = (v,i)=>{
        let burl;
        if(!(v||i)){
            setDefStatus(true);
            burl ='https://'+dest+(dest === "infftrhkuat.aswatson.net"?"/":':9002/')
            return setURL(burl)
        }

        let des = i === 'dest'? v:dest;
        let pc = i === 'pc'? v:PC;
        let n = i === 'number'? v:cnum;
        // setDest or PC
        if(i==='number'){
            setDefStatus(false);
            setNumber(v);
        }
        if(i==='pc'){
            setPC(v);
            setDefStatus(false);
            if(v==='p')
            {
                setProdStatus(false);
                setCatStatus(true);
            }

            else if(v==='ct')
            {
                setCatStatus(false);
                setProdStatus(true);
            }
            else{
                setProdStatus(true);
                setCatStatus(true);
            }
        }

        if(i==='dest'){
            setDest(v);
            setDefStatus(true);
            pc = n = '';
        }
        burl ='https://'+des +(des === "infftrhkuat.aswatson.net"?"/":':9002/')+pc+((n && ((cnum && (v === 'p'||v ==='ct'))||i==='number'))?'/'+n:'');
        setAccess(adminPoint.includes(des)?false:true);
        setURL(burl)
    }
    const newTab = ()=>{
        window.open(url, '_blank').focus();
    }
    return(
        <div id="linkHopper">
            <div id="urlPreview">
                <input type="text" id="currentIP" defaultValue={url} value={url} onChange={(e)=>{setURL(e.currentTarget.value)}}/>
                <button id='currentIP' onClick={(e)=>{newTab()}}>Go</button>
            </div>
            <div id="urlOption">
                <div>
                    <input type="radio" name="precade" id="default" onChange={(e) => {URLBuilder()}} checked={def} defaultChecked={true}/>
                    <label for="default">Default</label>
                </div>
                <div>
                    <input type="radio" name="precade"  id="backoffice" onChange={(e)=>{URLBuilder('backoffice','pc')}} disabled={access}/>
                    <label for="backoffice">Backoffice</label>
                </div>
                <div>
                    <input type="radio" name="precade" id="hac" onChange={(e) => {URLBuilder('hac', 'pc')}} disabled={access}/>
                    <label for="hac">hac</label>
                </div>
                <div>
                    <input type="radio" name="precade" id="Product" value="" onChange={(e)=>{URLBuilder('p','pc')}}/>
                    <label for="Product">Product</label>
                    <span><input type="text" name="productID" value={!prodStatus?cnum:''}  onChange={(e)=>{URLBuilder(e.currentTarget.value,'number')} } disabled={prodStatus}/></span>
                </div>
                <div>
                    <input type="radio" name="precade" id="Category" value='' onChange={(e)=>{URLBuilder('ct','pc')}}/>
                    <label for="Category">Category</label>
                    <span><input type="text" name="productID" value={!catStatus?cnum:''}  onChange={(e)=>{URLBuilder(e.currentTarget.value,'number')} } disabled={catStatus}/></span>
                </div>
            </div>
            <div id="lister">
                <h3>List of FTRHK IP</h3>
                <div id="urlPicker">
                    {
                        ip.map(
                            (x,i)=>
                        {
                            var domclass = "ipCol";
                            var ipName = x;
                            ipName += adminPoint.includes(x)?"(ADMIN)":"";
                            ipName += uatPoint.includes(x)?"(UAT)":"";
                            ipName += sitPoint.includes(x)?"(SIT)":"";
                            domclass += mainPoint.includes(x)?" mainpoint":""
                            domclass += uatPoint.includes(x)?" uatpoint":""
                            domclass += sitPoint.includes(x)?" sitpoint":"";
                            return(
                                <div className={domclass}
                                     data-ip-value={x}
                                     onClick={(e)=>{URLBuilder(e.currentTarget.dataset.ipValue,'dest')}}>{ipName}</div>
                            )
                        }
                        )}
                </div>
            </div>
        </div>
    );
}