import React from 'react';

export default function PartialCancel(){
    const [flex,setFlex] = React.useState('');
    const [groo,setGroo] = React.useState('');

    const copyToClipboardClick = (e)=>{navigator.clipboard.writeText(e.currentTarget.value)};
    const changeList = (e)=>{
        if(e.currentTarget.value === null) return;
        let listSMP = e.currentTarget.value.split('\n').filter(x=>x!='').map(x=>{return `'${x}'`});

        setFlex(`SELECT {smpStore} || '/' ||{smptill} || {smptillseqnumber}
            ,{code}
            ,{os.code}
        FROM {Order AS o
            JOIN OrderStatus AS os ON {o.status} = {os.pk}}
        WHERE {smpStore} || '/' ||{smptill} || {smptillseqnumber} IN (${listSMP.join()})`)
        setGroo(
            `def orderCodes = [${listSMP.join().replace(/\//gm,'').replace(/\'/gm,'"')}]
for (code in orderCodes) {
  def c = ftrConsignmentService.findConsignmentBySmpOrderNo(code)
  def order = c.getOrder()
  println code
  println c.getStatus()
  println order.getStatus()
  if ("CANCELLED".equals(c.getStatus().getCode()) && "CANCELLED".equals(order.getStatus().getCode())) {
    modelService.remove(c);
    for(orderProcess in order.getOrderProcess()) {
      if(orderProcess.getProcessDefinitionName().contains("igcOrderFulfillment")) {
        println orderProcess;
        businessProcessService.restartProcess(orderProcess, "splitOrderAction");
      }
    }
  } else {
    println code + ": Wrong Status: Consignmrnt: " + c.getStatus()
    println code + ": Wrong Status: Order: " + order.getStatus()
    println code + ": Wrong Status: Order Return Request: " + order.getReturnRequests()
  }
  println "=========================="
}`

        );
    }
    return(
        <div id="partialCancel">
            <h2>Enter newline-split SMP order for partial cancel.</h2>
            <textarea className="input" onChange={changeList}></textarea>
            <div class="textio">
                <textarea value={flex} onClick={copyToClipboardClick}></textarea>
                <textarea value={groo} onClick={copyToClipboardClick}></textarea>
            </div>
        </div>
    );
}