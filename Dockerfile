FROM node

WORKDIR /ftrhkquickapp

COPY . /ftrhkquickapp

RUN npm install -g serve
CMD serve -p 1041 build